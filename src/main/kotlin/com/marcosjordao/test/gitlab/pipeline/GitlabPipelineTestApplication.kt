package com.marcosjordao.test.gitlab.pipeline

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GitlabPipelineTestApplication

fun main(args: Array<String>) {
	runApplication<GitlabPipelineTestApplication>(*args)
}
